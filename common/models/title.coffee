module.exports = (Title) ->
	#TODO - implement inferring standard validations from model config file.
	titleMinLength = 1
	titleMaxLength = 256
	descriptionMaxLength = 65536

	Title.validatesPresenceOf 'title'
	Title.validatesLengthOf 'title',
		min: titleMinLength
		max: titleMaxLength
	Title.validate 'description', (err) ->
		if @description and @description.length > descriptionMaxLength
			err 'Description is too long'
