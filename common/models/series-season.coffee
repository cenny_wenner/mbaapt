#TODO: Introduce parent to Season and Series
module.exports = (SeriesSeason) ->
	elementSuffixMaxLength = 50

	SeriesSeason.validatesPresenceOf 'seriesId', 'seasonId', 'inSeriesIndex'
	SeriesSeason.validate 'seriesId', () ->
		if @seriesId and @seriesId < 0
			err 'seriesId cannot be negative'
	SeriesSeason.validate 'seasonId', () ->
		if @seasonId and @seasonId < 0
			err 'seasonID cannot be negative'
	SeriesSeason.validate 'inSeasonIndex', () ->
		if @inSeriesSuffix and @inSeriesSuffix < 0
			err 'inSeasonIndex cannot be negative'
	SeriesSeason.validate 'elementIndex', () ->
		if @elementIndex and @elementIndex < 0
			err 'elementIndex cannot be negative'
	SeriesSeason.validate 'elementSuffix', () ->
		if @elementSuffix and @elementSuffix.length > elementSuffixMaxLength
			err 'elementSuffix too long'
	
	