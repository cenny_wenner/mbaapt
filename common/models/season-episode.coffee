#TODO: Check that the given ids match existing objects.
#TODO: Delete upon deletion of either associated object.
#TODO: Perhaps we should keep the indexes continuous.

module.exports = (SeasonEpisode) ->
	elementSuffixMaxLength = 50

	SeasonEpisode.validatesPresenceOf 'seasonId', 'episodeId', 'elementIndex'
	SeasonEpisode.validate 'seasonId', () ->
		if @seasonId and @seasonId < 0
			err 'seasonID cannot be negative'
	SeasonEpisode.validate 'episodeId', () ->
		if @episodeId and @episodeId < 0
			err 'episodeID cannot be negative'
	SeasonEpisode.validate 'elementIndex', () ->
		if @elementIndex and @elementIndex < 0
			err 'elementIndex cannot be negative'
	SeasonEpisode.validate 'elementSuffix', () ->
		if @elementSuffix and @elementSuffix.length > elementSuffixMaxLength
			err 'elementSuffix too long'
