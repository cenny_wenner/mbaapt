_ = require 'lodash-node'

module.exports = (Program) ->
	secondsDurationMin = 0
	secondsDurationMax = 10 * 365 * 24 * 3600
	genresMaxLength = 100
	genresElementMaxLength = 50
	castMaxLength = 100
	castElementMaxLength = 100

	Program.validate 'secondsDuration', (err) ->
		throw "Errr"
		if @secondsDuration
			if @secondsDuration < secondsDurationMin
				err 'SecondsDuration too small'
			else if @secondsDuration > secondsDurationMax
				err 'SecondsDuration too large'
	Program.validate 'cast', (err) ->
		if @cast
			if @cast.length > castMaxLength
				err 'Cast list is too long'
			else if _.find(@cast, (x) -> !x) #Note: @cast is not a js Array
				err 'Cast list cannot contain empty elements'
			else if _.find(@cast, (x) -> x.length > castElementMaxLength)
				err 'Cast list element too long'
	Program.validate 'genres', (err) ->
		if @genres
			if @genres.length > genresMaxLength
				err 'Genres list is too long'
			else if _.find(@genres, (x) -> !x) #Note: @genres is not a js Array
				err 'Genres list cannot contain empty elements'
			else if _.find(@genres, (x) -> x.length > genresElementMaxLength)
				err 'Genres list element too long'

