require('coffee-script/register');

var app = require('./app');

// start the server if `$ node server.js`
if (require.main == module)
	app.start();
