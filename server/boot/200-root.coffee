# Route status to '/' and '/status'.

#TODO: Document.

module.exports = (server) ->
	# Install a `/` route that returns server status
	router = server.loopback.Router()
	#TODO put in server config.
	router.get '/status', server.loopback.status()
	server.use router
