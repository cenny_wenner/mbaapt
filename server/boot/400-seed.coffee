#TODO: move to command?
#TODO: move to json?
#TODO: use a generator script like faker?
#TODO: only if development?

module.exports = (app) ->
	Movie = app.models.Movie
	Episode = app.models.Episode
	SeasonEpisode = app.models.SeasonEpisode
	Season = app.models.Season
	SeriesSeason = app.models.SeriesSeason
	Series = app.models.SeriesSeason

	#TODO check type
	addEpisodeToSeason = (ep, seas) ->
		SeasonEpisode.create
			episodeId: ep.id
			seasonId: seas.id
	addSeasonToSeries = (seas, series) ->
		SeriesSeason.create
			seasonId: seas.id
			seriesId: series.id
	
	movie1 = Movie.create
		title: "Sleeping Brit"
		description: "In this revolutionary film, John Cleese releases a record of himself sleeping for a full 6.279 hours."
		secondsDuration: Math.round 6.279*3600
		genres: ["Brittish humor", "Comedy", "Our Lord John Cleese"]
		cast: ["John Cleese"],
		releaseDate: new Date 2011, 10, 20
	movie2 = Movie.create
		title: "Sleeping Stralien"
		description: "This movie description can not be viewed in your region due to licensing claims."
		secondsDuration: Math.round 8*3600
		genres: ["Drama"]

	onceSeries = Series.create
		title: "Once upon"
	onceSeason1 = Season.create
		title: "Once upon tales"
	addSeasonToSeries onceSeason1, onceSeries
	weirdSeries = Series.create
		title: "Weird"
	weirdSeason1 = Season.create
		title: "Weird"
	addSeasonToSeries weirdSeries, weirdSeason1
	weirdSeasonSpecials = Season.create
		title: "Weirdier"
	addSeasonToSeries weirdSeries, weirdSeasonSpecials

	onceS1E1 = Episode.create
		title: "Once upon a time"
		description: "I forgot the rest of the story."
	addEpisodeToSeason onceS1E1, onceSeason1

	weirdS1E1 = Episode.create
		title: "What?"
		description: "A writer comes to terms with that he's writing an episode."
	addEpisodeToSeason weirdS1E1, weirdSeason1

	onceWhatEpisode = Episode.create
		title: "Once upon what?"
		description: "Cross-over of the famous episodes What? and Once Upon a Time."
	addEpisodeToSeason onceWhatEpisode, onceSeason1
	addEpisodeToSeason onceWhatEpisode, weirdSeasonSpecials

