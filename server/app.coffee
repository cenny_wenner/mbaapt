loopback = require 'loopback'
boot = require 'loopback-boot'
app = module.exports = loopback()

app.start = () ->
  # start the web server
  return app.listen () ->
    app.emit 'started'
    baseUrl = app.get('url').replace(/\/$/, '')
    humanBaseUrl = baseUrl.replace(/0\.0\.0\.0/,'\/127.0.0.1:')
    console.log 'Web server listening at: %s', humanBaseUrl
    if app.get 'loopback-component-explorer'
      explorerPath = app.get('loopback-component-explorer').mountPath
      console.log 'Browse your REST API at %s%s', humanBaseUrl, explorerPath

# Bootstrap the application, configure models, datasources and middleware.
# Sub-apps like REST API are mounted via boot scripts.
boot app, __dirname, (err) -> throw err if err
