# MBaaPT - Movie Backend as a Programming Test
Simple movie (metadata) REST backend for a programming test.

## Installation
This project requires *NodeJS* and *npm* - the Node Package Manager.
After installing *npm*, the project can be installed by running `npm install`
from the project root. The project has been tested with *NodeJS 0.10.42* and
*npm 3.7.2*.

To improve performance of *npm install*, one may want to disable the
progress bar: `npm set progress=false`.

### Linux
Install `npm` via your package manager. This should also `node` in the process.
On some distros, an additional installation of the `node`, `nodejs`, or `nodejs-legacy`
package may be required. For distros which are more than two years old, one may
have to manually download and install the relevant packages.

Due to package-naming conflicts, one should try to run `node` after
installation. If it fails but `nodejs` works, issue the following
command to link `node` to `nodejs`:
```
sudo update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10
```

### Windows
Download and install NodeJS from [nodejs.org](http://nodejs.org). Check that your
installation successfully installed my running `node` and `npm` from a command prompt.

## Running
The development server can be started by running `node .` from the project root.
The default port is 3000.

The project can also be deployed for production by first running the build script,
`./build-deployment`, which should handle any native extensions. Following this,
one runs `./deploy` to start a multithreaded server.

The server processes can safely be terminated with `Ctrl+C`.

## API
The default server port is 3000 and the default API endpoint is `/api`.

With the server running, a Swagger specification of the API can be accessed
at `/explorer`. Running locally, this corresponds to
[localhost:3000/explorer](http://localhost:3000/explorer).

### Example
As an example, when running locally, the following command retrieves
a specific movie:
```
GET localhost:3000/api/Movies/2
```
By default, the response will be in JSON: 
```
{
  "secondsDuration": 1500,
  "releaseDate": "2016-03-01T00:00:00.000Z",
  "genres": [],
  "cast": [
    "A Cat"
  ],
  "title": "Showering cat",
  "description": "A cat gets an unintended bath.",
  "deleted": false,
  "id": 1
}
```