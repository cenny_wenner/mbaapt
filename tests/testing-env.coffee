# Helpers for initializing testing.
#TODO: Remove globals.
module.exports =
	initialize: () ->
		global.gutil = require 'gulp-util'
		global.chai	= require 'chai'
		global.assert 	= chai.assert
		global.expect 	= chai.expect
		global.should 	= chai.should()
		global.request 	= require 'supertest'
		global.loopback 	= require 'loopback'
		global.boot 	= require 'loopback-boot'
		global.getApp 	= () ->
			app = loopback()
			boot app, serverRoot, (err) -> throw err if err
			app
		