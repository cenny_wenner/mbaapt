initializeTesting()

describe 'the Season-Episode model relation', () ->
	Season = null
	Episode = null
	SeasonEpisode = null
	
	before () ->
		app = getApp()
		Season = app.models.Season
		Episode = app.models.Episode
		SeasonEpisode = app.models.SeasonEpisode

	it 'should only require a title, valid season and episode', () ->
		#TODO

	describe 'the season relation', () ->
		#it 'should require a valid season', () ->
		#	#TODO

		it 'should get its associated season', () ->
			#TODO

	describe 'the episode relation', () ->
		#it 'should require a valid episode', () ->
		#	#TODO

		it 'should get its associated episode', () ->
			#TODO

	describe 'the element index', () ->
		it 'should accept and set correctly', () ->
			#TODO

		it 'should be nonnegative', () ->
			#TODO

		#NOTE: questionable if we really should have these since they rely on tested parts.
		#      it would merely be a check now such that if anyone changes properties, they would
		#      have to change in more than one place.
		it 'should be set by deault', () ->
			#TODO

		it 'should be unique', () ->
			#TODO

	describe 'the name suffix', () ->
		it 'should be accepted and set correctly', () ->
			#TODO

		it 'should have a maximum length', () ->
			#TODO

	#TODO move to an entity superclass and test there.
	describe 'the deleted field', () ->
		it 'should be accepted and set correctly', () ->
			#TODO

		it 'should be false by default', () ->
			#TODO

		###
		it 'should be set when season dependency is soft deleted', ->
			#TODO
		
		it 'should be set when season dependency is hard deleted', ->
			#TODO
		
		it 'should be set when episode dependency is soft deleted', ->
			#TODO
		
		it 'should be set when episode dependency is hard deleted', ->
			#TODO
		###
	
	describe 'the episodes', ()->
		it 'should get its Seasons', ()->
			#TODO

		it 'should get its SeasonEpisodes', (done)->
			# Not the right test
			episodeData = title: "Another day, another cameo"
			seasonData = title: "First season"
			Episode.create episodeData, (err, episode) ->
				return done err if err
				assert.isObject episode
				Season.create seasonData, (err, season) ->
					return done err if err
					assert.isObject season
					SeasonEpisode.create
						episodeId: episode.id
						seasonId: season.id
						(err, seasonEpisode) ->
							return done err if err
							assert.isObject seasonEpisode
							SeasonEpisode.findOne
								where: {id: seasonEpisode.id}
								include: ['episode', 'season']
								(err, res) ->
									return done if err
									assert.isObject res
									assert.isFunction res.episode
									resEpisode = res.episode()
									assert.isObject resEpisode
									resEpisode.title.should.equal episodeData.title
									done()

	describe 'the seasons', ()->
		it 'should get its Episodes', ()->
			#TODO

		it 'should get its SeasonEpisodes', ()->
			#TODO