initializeTesting()

describe 'the Episode model', () ->
	Model = null
	
	before () ->
		Model = getApp().models.Episode

	#TODO move to utility file
	expectValidationError = (err) ->
		assert.isObject err, "Expected ValidationError"
		assert.equal err.name, 'ValidationError', 'Expected ValidationError'

	#TODO have inheritance test so that we do not need to repeat these.
	it 'should only require a title', (done) ->
		newInstanceData = title: "Once upon a time"
		Model.create newInstanceData, (err, instance) ->
			return done err if err
			assert.isNotNull instance
			done()

	describe 'the inherited title field', () ->
		it 'should be required', (done) ->
			newInstanceData = {}
			Model.create newInstanceData, (err,res) ->
				expectValidationError err
			done()



