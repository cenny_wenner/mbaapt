initializeTesting()

#TODO - we should have an abstracted model for SeriesSeason and SeasonEpisode
#       that we test and eliminate much of the need to repeat tests here.
describe 'the Series-Season model relation', () ->
	Series = null
	Season = null
	SeriesSeason = null
	
	before () ->
		app = getApp()
		Series = app.models.Series
		Season = app.models.Season
		SeriesSeason = app.models.SeriesSeason

	it 'should only require a title, valid series and season', () ->
		#TODO

	describe 'the series relation', () ->
		it 'should require a valid series', () ->
			#TODO

		it 'should get its associated series', () ->
			#TODO

	describe 'the season relation', () ->
		it 'should require a valid season', () ->
			#TODO

		it 'should get its associated season', () ->
			#TODO

	describe 'the element index', () ->
		it 'should accept and set correctly', () ->
			#TODO

		it 'should be nonnegative', () ->
			#TODO

		it 'should be set by deault', () ->
			#TODO

		it 'should be unique', () ->
			#TODO

	describe 'the name suffix', () ->
		it 'should be accepted and set correctly', () ->
			#TODO

		it 'should have a maximum length', () ->
			#TODO

	#TODO move to an entity superclass and test there.
	describe 'the deleted field', () ->
		it 'should be accepted and set correctly', () ->
			#TODO

		it 'should be false by default', () ->
			#TODO

		###
		it 'should be set when series dependency is soft deleted', ->
			#TODO
		
		it 'should be set when series dependency is hard deleted', ->
			#TODO
		
		it 'should be set when season dependency is soft deleted', ->
			#TODO
		
		it 'should be set when season dependency is hard deleted', ->
			#TODO
		###

	describe 'the season', ()->
		it 'should get its Series', ()->
			#TODO

		it 'should get its SeriesSeasons', ()->
			#TODO

	describe 'the series', ()->
		it 'should get its Seasons', ()->
			#TODO

		it 'should get its SeriesSeasons', ()->
			#TODO
