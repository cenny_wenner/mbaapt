initializeTesting()

describe 'the Title model', () ->
	Model = null
	
	before () ->
		Model = getApp().models.Title

	expectValidationError = (err) ->
		assert.isObject err, "Expected ValidationError"
		assert.equal err.name, 'ValidationError', 'Expected ValidationError'

	#TODO abstract error-catching for all its
	it 'should only require a title', (done) ->
		newInstanceData = title: "Once upon a time"
		Model.create newInstanceData, (err, instance) ->
			return done err if err
			assert.isNotNull instance
			done()
	
	#TODO move to utility file
	makeStringOfLength = (len) ->
		new Array(len+1).join 'a'

	#Q. Rewrite some of these tests using .isValid()?
	describe 'the title field', () ->
		#TODO: extract properties from model definition?
		titleMinLength = 1
		titleMaxLength = 256

		# TODO need an error wrapper for all test methods.
		it 'should be required', (done) ->
			newInstanceData = {}
			Model.create newInstanceData, (err,res) ->
				expectValidationError err
			done()

		it 'should be set correctly', (done) ->
			newInstanceData = title: "Once upon a time"
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.title.should.equal newInstanceData.title
				done()

		#TODO: abstract min and max length tests
		it "should have minimum length #{titleMinLength}", (done) ->
			# This indeed produces a string of length titleMinLength-1 due to join().
			titleTooShort = new Array(titleMinLength).join 'a'
			titleOfMinLength = titleTooShort + 'a'
			Model.create {title:titleOfMinLength}, (err, instance) ->
				return done err if err
				assert.isNotNull instance.title
				Model.create {title:titleTooShort}, (err, instance) ->
					expectValidationError err
					done()

		it "should have maximum length #{titleMaxLength}", (done) ->
			# This indeed produces a string of length titleMaxLength due to join().
			titleOfMaxLength = new Array(titleMaxLength+1).join 'a'
			titleTooLong = titleOfMaxLength + 'a'
			Model.create {title:titleOfMaxLength}, (err, instance) ->
				return done err if err
				assert.isNotNull instance.title
				Model.create {title:titleTooLong}, (err, instance) ->
					expectValidationError err
					done()

	describe 'the description field', () ->
		it 'should be set correctly', (done) ->
			newInstanceData = title: "Title", description: "There was a cat in a hat."
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.description.should.equal newInstanceData.description
				done()

	describe 'the deleted field', () ->
		deletedDefault = false

		it 'should default to false', (done) ->
			newInstanceData = title: "Title"
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.deleted.should.equal deletedDefault
				done()

		it 'should be set correctly', (done) ->
			newInstanceData = title: "Title", deleted: !deletedDefault
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.deleted.should.equal newInstanceData.deleted
				done()

		#TODO: test other properties...


