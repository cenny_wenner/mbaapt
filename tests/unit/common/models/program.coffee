initializeTesting()

describe 'the Program model', () ->
	Model = null
	
	before () ->
		Model = getApp().models.Program

	#TODO move to utility file
	expectValidationError = (err) ->
		assert.isObject err, "Expected ValidationError"
		assert.equal err.name, 'ValidationError', 'Expected ValidationError'

	#TODO only test inheritance and be done with it.
	it 'should only require a title', (done) ->
		newInstanceData = title: "Once upon a time"
		Model.create newInstanceData, (err, instance) ->
			return done err if err
			assert.isNotNull instance
			done()

	describe 'the inherited title field', () ->
		it 'should be required', (done) ->
			newInstanceData = {}
			Model.create newInstanceData, (err,res) ->
				expectValidationError err
			done()
	
	describe 'the inherited description field', () ->
		it 'should be set correctly', (done) ->
			newInstanceData = title: "Title", description: "There was a cat in a hat."
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.description.should.equal newInstanceData.description
				done()

	#Q. Rewrite some of these tests using .isValid()?
	describe 'the secondsDuration field', () ->
		#TODO: extract properties from model definition?
		secondsDurationMin = 0
		secondsDurationMax = 10 * 365 * 24 * 3600

		it 'should be set correctly', (done) ->
			newInstanceData = title: "Title", secondsDuration: 425
			Model.create newInstanceData, (err, instance) ->
				return done err if err
				instance.secondsDuration.should.equal newInstanceData.secondsDuration
				done()

		it "should have minimum #{secondsDurationMin}", (done) ->
			Model.create {title:'a', secondsDuration:secondsDurationMin}, (err, instance) ->
				return done err if err
				assert.isNotNull instance.secondsDuration
				Model.create {title:'a', secondsDuration:secondsDurationMin-1}, (err, instance) ->
					expectValidationError err
					done()

		it "should have maximum #{secondsDurationMax}", (done) ->
			Model.create {title:'a', secondsDuration:secondsDurationMax}, (err, instance) ->
				return done err if err
				assert.isNotNull instance.secondsDuration
				Model.create {title:'a', secondsDuration:secondsDurationMax+1}, (err, instance) ->
					expectValidationError err
					done()

	#TODO: test other properties...


