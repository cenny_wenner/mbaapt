initializeTesting()

describe 'the server status', () ->
	app = null
	statusAddress = '/status' #Todo: load from server config
	
	beforeEach () ->
		app = getApp()

	it 'should respond with JSON', (done) ->
		request app
			.get statusAddress
			.set 'Accept', 'application/json'
			.expect 200
			.end (err, res) ->
				return done(err) if err
				resp = JSON.parse res.text
				done()

	it 'should report almost-0 uptime', (done) ->
		request app
			.get statusAddress
			.set 'Accept', 'application/json'
			.expect 200
			.end (err, res) ->
				return done(err) if err
				resp = JSON.parse res.text
				assert.isAtMost resp.uptime, 1.0, "uptime on new App should be roughly 0"
				done()
	
	# A number of other possible tests, but seems like an overkill for
	# this functionality.

