initializeTesting()

describe 'Chai', () ->
  it 'should support expect.to syntax', () ->
    expect(1+1).to.equal 2
    expect('hello').to.be.a 'string'
    'hello'.should.be.a 'string'

#...
