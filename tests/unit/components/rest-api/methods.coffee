###

Testing of Loopback REST API.

Three possible levels of testing are as follows.
a) Test only that the API is properly attached to an endpoint.
   Since this is core library functionality, save version upgrading, this should suffice as
   test that the REST API Methods work correctly.
   (This option should reside in unit/server/boot)
b) Test every type of API endpoint.
   The approach in a) could be considered suspectible to library-specification changes, e.g.
   when upgrading version, as well as to unintended changes to the server, such as
   routing patterns. A somewhat stronger test suite, and also something more aking to a
   behavior specificiation, would involve making a suite of tests for every API endpoint,
   e.g. GET <Model>s, GET /<Model>s/:id, POST <Models>/:id, etc.
c) Test every API endpoint of every model.
   The most 

For rapid iteration, a) should suffice. For enterprise production, c) is desired.
We demonstrate b) below.

###

initializeTesting()

describe 'the REST API', () ->
	app = null

	#TODO - Replace with a newly created model which is known not to have general limitations.
	modelAPIAddress = '/api/Titles'
	
	beforeEach () ->
		app = getApp()

	describe 'the GET method', () ->
		it 'should respond with JSON', (done) ->
			request app
				.get modelAPIAddress
				.set 'Accept', 'application/json'
				.expect 200
				.end (err, res) ->
					return done(err) if err
					JSON.parse res.text
					done()
		
		it 'should find instances', () ->
			#TODO

		#TOD other GET endpoints

	describe 'the POST method', () ->
		it 'should accept new instances', (done) ->
			newInstance =
				title: "My new title"
			request app
				.post modelAPIAddress
				.set 'Accept', 'application/json'
				.send newInstance
				.expect 200
				.end (err, res) ->
					return done(err) if err
					resp = JSON.parse res.text
					assert resp.title == newInstance.title
					done()

		# Check creation..
	
	# Other methods besides GET, POST

	# Try breaking the server - e.g., mass creation.
	