gulp 	= require 'gulp'
mocha 	= require 'gulp-mocha'
coffee 	= require 'gulp-coffee'
sourcemaps 	= require 'gulp-sourcemaps'
run 	= require 'run-sequence'
config 	= require './gulpconfig.json'
gutil 	= require 'gulp-util'
path 	= require 'path'
testingEnv 	= require './tests/testing-env'

#TODO refactor
global.appRoot	= path.resolve __dirname
global.serverRoot 	= appRoot + config.paths.serverRoot
global.initializeTesting = () ->
	testingEnv.initialize()

# TODO clean, changed only
gulp.task 'build', (done) -> run 'compile', 'test', done
gulp.task 'compile', ['coffee'], (done) -> done()

gulp.task 'coffee', (done) ->
	return gulp
		.src(config.paths.coffees, {base: "./"})
		.pipe(sourcemaps.init())
		.pipe coffee()
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./'))
		.on 'error', (err) -> throw err

# Rerun the task when a file changes
gulp.task 'watch', (done) ->
	#Todo: Should not terminate on error.
	return gulp.watch(config.paths.coffees, ['coffee'])
		.on 'error', (err) -> done err

gulp.task 'test', (done) ->
	return gulp.src(config.paths.tests)
		.pipe(mocha({ reporter: 'spec' }))
		.on 'error', (err) -> done err
