# Exercise Choices and Reflections
## Loopback
TODO

### Performance
If performance was the primary concern, NodeJS would not be the best choice. In particular, Sparkjava and Go-based frameworks both outperform NodeJS-based frameworks with great margins. However, NodeJS is still considered one of the fastest backend choices, consistently outperforming PHP, Rails, Python-based frameworks, and most Java-based frameworks. Though it may not be the top choice for performance, it is certainly not a cause for worry.

## Node modules in Git
It may seem naive to replicate managed dependencies to a CVS but in
my humble opinion, it carries with it more positives than negatives.
The bandwidth impact and such are minimal;
one can clean modules when needed for production deployment with a single command;
most node packages are platform independent while native dependencies are nevertheless handled when executing `npm install`;
one does not need to worry about whether packages will be around or if some unintended upgrade will break the system; and much more.
That there will be occasional commits with package upgrades is a minor concern,
and I would rather see it a controlled process.

TODO: Move to a submodule, subtree, or similar to hide from normal history.
For `git grep`, for now, one should instead run `git grep [...] -- . ':!node_modules'`.

## CoffeeScript
The community is rather divided on this. Since the exercise is meant to show of our coding skills, my preference is to use it. It makes code more concise, which in my opinion makes it clearer. Some would disagree.

## Data modelling
We have chosen not to implement every possible datum which may be associated with the requested resources. Instead, we have chosen
a subset which captures as many different types of data as possible.

### Flat vs object structure
One might wonder whether separate series and season objects are even necessary. A season is simply an ordered collection of shows with metadata, and so is a series. To make matters worse, there are also movie series and tv-show series, which do not have an explicit season. Naturally, one can always enforce such series to have a blanket first and only season, but it seems a bit artificial.

What use would an object structure have? The objects are useful if we wish to attach methods to them. For example, suppose that we wanted aggregated durations, casts, genres for a whole series. What we would want then are precisely objects so that the different cases are handled without us needing to know the type. So as not to complicate the model unnecessarily, we will keep in mind the parents of these extensions but not implement them.

We are skipping utility fields such as created, updated, version.

### Title
Introduced as an object having a title and description.

* title: required string
* description: optional string
* deleted: boolean default false

### Program < Title
Introduced as a parent to both *Movie* and *Show*, since this generalization is to be expected.
The fields we have chosen to include are:

* duration: optional duration
* releaseDate: optional date-time
* genres: string collection default empty
* cast: string list default empty. (Ideally this should be a tagged list: "Leonardo Dicaprio" as "The Lamp Post")

### Movie < Program
No new fields. Perhaps add 'related movies', perhaps add a 'MovieSeries' class.

### Episode < Program
Nothing added to the program class.

### Season < Title
An ordered list of Episodes. This ordered list may be tagged as the one often with seasons wish to describe episodes with their "episode number", which need not be a number. Such as "Game of Chairs Season 2, Episode 4-5: Backbreaking developments". In this example, "Game of Chairs" is the series title, "2" is the season number, "4-5" is the episode number, and "Backbreaking developments" is the episode title.

* episodes: tagged Epiode list default empty. (Ideally this should be a tagged list: < Episode:Pilot> as "0", < Episode:How it all started> as "1-2")

### Series < Title
An ordered list of Seasons. Again, there may be a (possibly non-numeric) index associated with each season in the list.

* seasons: tagged Season list default empty (< Season:A new tomorrow> as "1", < Season:Specials> as "Specials")
