# Developer Readme
Additional information for project developers.

## Gulp
The following tasks are essential to development.

* `./gulp build` - Rebuild project and run automated tests.
* `./gulp watch` - Automatically recompile files on change.
* `./gulp test` - Run automated tests. On `gulp watch`, this does not require recompilation.

## StrongLoop Controller
The command `./slc` provides development and server-management options beyond
running `node .`. Including deployment, status, generators, debugging, profiling, and
process management. Deployment as a docker is also possible. We refer the reader to the
`slc` documentation.

## Deploying
To build and deploy the project, one should first run `gulp build`. After building, one can run a
multithreaded production-mode server with `./slc run`. This process is also handled by the two
scripts, `./build-deployment` and `./deploy`. The default port is 3000 and can be changed in
`server/config.json`.

## Testing
The project uses the following libraries for automated testing and
behavior-driven development: *Mocha*, *Chai*, *Sinon*, *Mockery*, *Supertest*.
The reader is referred to the following succinct explanation and examples:
[The Node.js Way - Testing Essentials](http://fredkschott.com/post/2014/05/nodejs-testing-essentials).

## Known Issues

* `./slc` does not support all commands which a globally-installed `slc` does. Specifically, the generators fail.

* `./slc build` does not work as intended presently, cleaning the present *Git* branch of non-production modules, requiring
a reexeution of `npm install`.

* Cloning the master branch presently corresponds to "stable development version", not "release". For instance, various .js files are not cloned
  in the process and one must run `gulp build` before deployment. Worse yet, the `deploy` branch does not include these files either.
